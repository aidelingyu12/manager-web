// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import './plugins/element'
import './assets/css/global.css'
import axios from 'axios'
import InnerObject from '../static/config.js'


//修改config目录下的index.js,方便修改后端访问目录
//index: path.resolve(__dirname, '../dist/index.html'),
//assetsRoot: path.resolve(__dirname, '../dist'),
//assetsSubDirectory: 'static',
//assetsPublicPath: './',

Vue.config.productionTip = false

//引入axios
Vue.prototype.$axios = axios

//挂在axios路径
//axios.defaults.baseURL = 'http://localhost:8087/api'

//axios.defaults.baseURL = a.ServerIp
axios.defaults.baseURL = InnerObject.InnerObject
//console.log(axios.defaults.baseURL)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
