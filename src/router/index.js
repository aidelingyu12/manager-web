import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import Login from '@/components/Login/login'
import Home from '@/components/home'
import Header from '@/components/Menus/header'
import SiderBar from '@/components/Menus/siderBar'
import Test from '@/components/test'
import DashBoard from '@/components/Menus/dashBoard'
import User from '@/components/Authority/user'
import Role from '@/components/Authority/role'
import PersonalInfo from '@/components/Authority/personalInfo'
import Community from '@/components/Authority/community'
import EasyPipe from '@/components/Devops/EasyPipe'
import pipeExecution from '@/components/Devops/pipeExecution'
import Topic from '@/components/Topic/topic'
import infiniteScroll from 'vue-infinite-scroll'

Vue.use(Router)
Vue.use(infiniteScroll)

const routes = [
  {
    path: '/',
    component: Login
  },
  {
    path: '/hello',
    name: 'HelloWorld',
    component: HelloWorld
  },
  {
    path: '/header',
    name: 'header',
    component: Header
  },
  {
    path: '/siderBar',
    name: 'siderBar',
    component: SiderBar
  },
  {
    path: '/home',
    redirect: 'dashBoard'
  },
  {
    path: '/home',
    component: Home,
    children: [
      {
        path: '/dashBoard',
        name: 'dashBoard',
        component: DashBoard
      },
      {
        path: '/user',
        name: 'User',
        component: User
      },
      {
        path: '/role',
        name: 'Role',
        component: Role
      },
      {
        path: '/personalInfo',
        name: 'PersonalInfo',
        component: PersonalInfo
      },
      {
        path: '/community',
        name: 'Community',
        component: Community
      },
      {
        path: '/easyPipe',
        name: 'EasyPipe',
        component: EasyPipe
      },
      {
        path: '/pipeExecution',
        name: 'pipeExecution',
        component: pipeExecution
      },
      {
        path: '/topic',
        name: 'topic',
        component: Topic
      }
    ]
  },
  {
    path: '/test',
    name: 'test',
    component: Test
  }
]

const router = new Router({routes: routes})

export default router
